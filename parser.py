

#!/bin/python3
import yaml
import re
import os

# Specify a list of cronjobs to be skipped from parsing
doNotParse = ['0hourly', 'katello-host-tools',
              'sysstat', 'katello', 'mdadm', 'popularity-contest']
# Specify possible users that can run cron jobs
users = ["root", "elasticsearch", "foreman", "puppet", "barman", "postgres"]


def run(host, template=[]):
    # read content of a file with cronjobs
    for _, _, files in os.walk(f'~/var/{host}/'):
        for name in files:
            if name in doNotParse:
                continue
            with open(f'~/var/{host}/{name}') as f:
                content = eval(f.read())
            # splitting all jobs by ;
            splitted = (content.get('crontab').split(';;'))
            for i in range(0, (splitted.count(''))):
                splitted.remove('')
            # making list of jobs like [1stjob,2ndjob,3rdjob]
            content.update(crontab=splitted)

            # checking if job is a comment
            if content.get('filename') == '':
                cron_file = ''
            else:
                cron_file = content.get('filename')
            temp_skip = False
            for each in content.get('crontab'):
                if temp_skip:
                    temp_skip = False
                else:
                    name = 'job'
                # if yes then it is a name of next job - Not sure about this cause there might be just human comments
                if re.findall("^#Ansible[:]{0,1}", each):
                    try:
                        name = each.split(': ')[1]
                    except Exception:
                        # drawback the name can content a lot of spaces
                        name = each.split(' ')[1]
                    temp_skip = True

                # check if cronjob is just VAR

                elif re.findall('^[A-Z]', each) or re.findall('^\#[A-Z]', each):
                    try:
                        name = each.split('=')[0]
                        disabled = 'no'
                        job = each.split('=')[1]
                        if str(name).startswith('#'):
                            name = each.split('=')[0][1:]
                            disabled = 'yes'
                        if cron_file:
                            template.append(
                                {'name': name, 'job': job, 'env': True, 'cron_file': cron_file, 'disabled': disabled})
                        else:
                            template.append(
                                {'name': name, 'job': job, 'env': True, 'disabled': disabled})
                    except Exception:
                        pass
                        # seems that this is comment, not VARS
                elif re.findall('^\*|^\d', each) or re.findall('^\#\d|^\#\*', each):
                    # check if user to be run is specified
                    if (each.split(' ', 6))[5] in users:
                        formatted = each.split(' ', 6)
                        user = formatted[5]
                        job = formatted[6]
                    else:
                        formatted = each.split(' ', 5)
                        job = formatted[5]

                    minute = formatted[0]
                    hour = formatted[1]
                    day = formatted[2]
                    month = formatted[3]
                    weekday = formatted[4]

                    if (str(minute).startswith('#')):
                        disabled = 'yes'
                        minute = str(formatted[0])[1:]
                    else:
                        disabled = 'no'
                    if content.get('filename'):
                        template.append({'name': name, 'job': job, 'user': user, 'minute': minute,
                                         'hour': hour, 'day': day, 'month': month, 'weekday': weekday, 'state': 'present',
                                         'cron_file': cron_file, 'disabled': disabled})
                    else:
                        template.append({'name': name, 'job': job, 'minute': minute,
                                         'hour': hour, 'day': day, 'month': month, 'weekday': weekday, 'state': 'present', 'disabled': disabled})

            # adding required fields to match the design
    formatted_dict = [{'cron_jobs': template}]
    i = 1
    with open(f'~/results/{host}/cron_auto.yml', 'w+') as f:
        data = yaml.dump(formatted_dict, f, sort_keys=False,
                         default_flow_style=False, explicit_start=True)
    with open(f'~/results/{host}/cron_auto.yml', 'r') as f:
        content = f.read()
    # removing extra brackets and intents
    fixed = content.replace(
        "- cron_jobs:", "cron_jobs:").replace("\'\"", "\'").replace("\"\'", "\'")
    with open(f'~/results/{host}/cron_auto.yml', 'w') as f:
        f.write(fixed)
    with open(f'~/results/{host}/cron_auto.yml', "r") as f:
        lines = f.readlines()
    for index, line in enumerate(lines):
        # adding newlines after each name ; i - is a WA to avoid eternal loop
        if i == 1:
            if line.startswith("  - name:"):
                lines.insert(index, "\n")
                i = 0
        else:
            i = +1
    with open(f'~/results/{host}/cron_auto.yml', "w") as f:
        contents = f.writelines(lines)
    print(
        f'See the result here - ~/results/{host}/cron_auto.yml')
