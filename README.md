# Make my Cron Jobs as IAC

## Table of contents:

1. [Description](#Description)
2. [Usage](#Usage)
2. [Result](#Result)

## 1. Description

<a name="Description"></a>
This script was developed to generate Ansible variable file that content all cronjobs of a host.
The script generates variable that meet ansible cron module (https://docs.ansible.com/ansible/latest/collections/ansible/builtin/cron_module.html)

## 2. Usage

<a name="Usage"></a>

1. Activate virtual env
2. Install requirements
3. run the script main.py from a machine that manages your hosts, e.g. ansible server.
4. Adapt script:
   - main.py - the entry point
        - Variables: playbook ; inventory  
   - cron.yml - a playbook that will generate raw data of a host's cronjobs.
        - Specify location of a place where cronjobs reside: play - check cron
   - parser.py - is a parser that will take raw data from cron.yml and make an ansible readable variable - results\server.local.domain\cron_auto.yml
        - Variables: playbook, users

## 3. Result

<a name="Result"></a>

See a generated yml file that can be consumed by ansible and that describes all host's cronjobs

![pic.png](./pic.png)
