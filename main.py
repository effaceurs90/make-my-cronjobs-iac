from subprocess import Popen, PIPE
import sys
from parser import run
import time

# bool to check if file is passed as arg
files = False

# Parameters

# Path to ansible playbook parser
playbook = "~/cron.yml"
# Path to your anbisle inventory file
inventory = "!/inventory"


def usage():
    print(
        """
   Usage:
   [--file] - specify a file with a list of hosts to be parsed
   [example1.com.local] - specify the host to be parsed
   """
    )
    exit()


if len(sys.argv) == 1:
    usage()
if sys.argv[1] == "--file":
    host = ""
    data = sys.argv[2]
    with open(data, "r") as f:
        content = f.readlines()
        for line in content:
            host += line.replace("\n", ",")
    host = host[: (len(host) - 1)]
    files = True
elif sys.argv[1]:
    host = sys.argv[1]
else:
    usage()

print("Running ansible job for host(s) - " + host)
p = Popen(
    f"ansible-playbook {playbook} -i  {inventory} -l {host}",
    shell=True,
    stdout=PIPE,
    stderr=PIPE,
)
out, err = p.communicate()

# for many hosts to check
if files:
    for each in host.split(","):
        print("Running parsing job for hosts - " + each)
        time.sleep(1)
        try:
            run(each, template=[])
        except Exception as err:
            print("ERROR " + str(err))
else:
    # for only one host to check
    print("Running parsing job for host - " + host)
    try:
        run(host, template=[])
    except Exception as err:
        print("ERROR " + str(err))
